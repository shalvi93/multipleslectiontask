//
//  TableViewCell.swift
//  MultipleSlectionTask
//
//  Created by Sierra 4 on 04/04/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit
import BEMCheckBox

class TableViewCell: UITableViewCell {

    @IBOutlet weak var labelOutlet: UILabel!
    
    @IBOutlet weak var ChkboxOutlt: BEMCheckBox!
    
    var DataArray : Data?
        {
        didSet
        {
            updateUI()
        }
        
    }
    fileprivate func updateUI()
    {

        
        labelOutlet?.text = DataArray?.name
        
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
