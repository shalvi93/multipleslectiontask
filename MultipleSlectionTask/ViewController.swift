//
//  ViewController.swift
//  MultipleSlectionTask
//
//  Created by Sierra 4 on 04/04/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit
import BEMCheckBox


class ViewController: UIViewController  {
    
    var SelectedList = [String]()
    var selectedItem = [Int]()
    
    
    @IBOutlet weak var tableviewOutlt: UITableView!
    
    var PlayListDetails  = [Data(name :"The Unhead Songs"),Data(name :"Hindi Songs "),Data(name :"Best of 2017 "),Data(name :"Tamil Songs "),Data(name :"English"),Data(name :"Dj "),Data(name :"Classic showcase"),Data(name :"Not So Bollywood"),Data(name :"Jazz"),Data(name :"HipHop")]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    override func prepare( for segue : UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "Segue"
        {
            let DestViewController : ViewController2 = segue.destination as! ViewController2
            DestViewController.delegate = self
            DestViewController.selctedPassed = SelectedList
        }
        
    }
    
    
    @IBAction func NextBtnActn(_ sender: Any) {
        print("Selected List : ",SelectedList)
        self.performSegue(withIdentifier: "Segue", sender: Any?.self)
    }
    
    
    
    @IBAction func btnAction(_ sender: UIControl) {
        
        if  !SelectedList.contains(PlayListDetails[sender.tag].name!){
            SelectedList.append(PlayListDetails[sender.tag].name!)
        }
        else
        {
            SelectedList.remove(at: SelectedList.index(of: PlayListDetails[sender.tag].name!)!)
        }
        print(SelectedList)
    }
    
    
}
extension ViewController : UITableViewDataSource  , UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return PlayListDetails.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        cell.DataArray = PlayListDetails[indexPath.row]
        cell.ChkboxOutlt.tag = indexPath.row
        
        if SelectedList.contains(PlayListDetails[indexPath.row].name!)
        {
            cell.ChkboxOutlt.setOn(true, animated: true)
            
        }
        else
        {
            cell.ChkboxOutlt.setOn(false, animated: false)
        }
        return cell
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
   
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
            SelectedList.remove(at: indexPath.row)
            tableviewOutlt.reloadData()
        }
        else
        {
        tableviewOutlt.reloadData()
    }
    }
}
extension ViewController:AddItemViewControllerDelegate{
    func getLabel(text: [String]) {
        //print("Text String :",text)
        SelectedList.removeAll()
        SelectedList = text
        tableviewOutlt.reloadData()
    }
}

